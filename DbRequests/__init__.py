#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb

from config import DB_PASSWORD, DB_HOST, DB_USER, DB_NAME
import utils


class DatabaseWrapperError(Exception):
    def __init__(self, reason):
        self._reason = reason

    def __str__(self):
        return "DatabaseWrapperError: {}".format(self._reason)


class MusicGameDatabase(object):
    def __init__(self):
        self._db = None
        self._cursor = None
        self._is_connected = False

    def _check_connection(self):
        if not self._is_connected:
            raise DatabaseWrapperError("Database is not connected")

    def connect(self):
        if not self._is_connected:
            self._db = MySQLdb.connect(
                host=DB_HOST,
                user=DB_USER,
                passwd=DB_PASSWORD,
                db=DB_NAME,
                charset='utf8'
            )
            self._cursor = self._db.cursor()
            self._is_connected = True
            utils.log_info("Connection to {} successful", DB_NAME)
        else:
            raise DatabaseWrapperError("Database is already connected")

    def close(self):
        self._check_connection()
        self._is_connected = False
        self._db.close()
        utils.log_info("Database disconnected")

    def commit(self):
        self._check_connection()
        self._db.commit()

    def init_tables(self):
        self._check_connection()
        self._cursor.execute(
            "CREATE TABLE IF NOT EXISTS players (" +
            "id INT NOT NULL AUTO_INCREMENT," +
            "vk_id TEXT NOT NULL," +
            "menu_name VARCHAR(50) DEFAULT \"FIRST_TIME\"," +
            "menu_context TEXT NOT NULL," +
            "PRIMARY KEY (id)" +
            ");"
        )
        self._cursor.execute(
            "CREATE TABLE IF NOT EXISTS music (" +
            "id INT NOT NULL AUTO_INCREMENT," +
            "name TEXT NOT NULL," +
            "vk_id TEXT NOT NULL," +
            "PRIMARY KEY (id)" +
            ");"
        )
        music = [
            ('Sawano Hiroyuki – aLIEz', '-157942650_456239025'),
            ('Guilty Crown – Bios (мика кобаяши ван лав)', '-157942650_456239024'),
            ('Darker than Black – Op', '-157942650_456239023'),
            ('Haiyore! Nyaruko-san – OP', '-157942650_456239022'),
            ('Mahou Shoujo Madoka Magica – Salve, Terrae Magicae', '-157942650_456239021'),
            ('Linked Horizon  – Guren no Yumiya', '-157942650_456239026'),
            ('JoJo – Sono Chi no Sadame', '-157942650_456239018'),
            ("JoJo's Bizarre Adventure - Bloody Stream", '-157942650_456239027'),
            ('God song', '-157942650_456239028'),
        ]
        self._cursor.executemany("INSERT INTO music (name, vk_id) VALUES (%s, %s);", music)
        utils.log_info("Tables were created")

    def get_player_info(self, vk_id):
        self._check_connection()
        self._cursor.execute("SELECT * FROM players WHERE vk_id = %s;", (vk_id,))
        return self._cursor.fetchone()

    def insert_player_info(self, vk_id, menu_name="FIRST_TIME", menu_context="{}"):
        self._check_connection()
        self._cursor.execute(
            "INSERT INTO players (vk_id, menu_name, menu_context) VALUES (%s, %s, %s);",
            (vk_id, menu_name, menu_context)
        )

    def update_player_info(self, user_id, menu_name, menu_context="{}"):
        self._check_connection()
        self._cursor.execute(
            "UPDATE players " +
            "SET menu_name = %s, menu_context = %s "
            "WHERE id = %s;",
            (menu_name, menu_context, user_id)
        )

    def get_random_songs(self, amount=4):
        self._check_connection()
        self._cursor.execute(
            "SELECT id, name, vk_id FROM music ORDER BY RAND() LIMIT %s;",
            (amount,),
        )
        return self._cursor.fetchall()

    def insert_song(self, song_name, vk_id):
        self._check_connection()
        self._cursor.execute(
            "INSERT INTO music (name, vk_id) VALUES (%s, %s);",
            (song_name, vk_id),
        )

    def get_unnamed_song(self):
        self._check_connection()
        self._cursor.execute("SELECT * FROM music WHERE name = %s;", ("empty", ))
        return self._cursor.fetchone()

    def update_song_info(self, music_id, name):
        self._check_connection()
        self._cursor.execute(
            "UPDATE music " +
            "SET name = %s " +
            "WHERE id = %s;",
            (name, music_id)
        )

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
