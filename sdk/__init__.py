#!/usr/bin/env python
# -*- coding: utf-8 -*-

import vk
from config import VK_API_TOKEN
import json
import DbRequests
import utils

session = vk.Session()
api = vk.API(session, v=5.0)


class MessageHandlerException(Exception):
    pass


class BaseMenu(object):
    type = "BASE_MENU"

    def __init__(self, user_id=None, context=None, db=None, message_info=None, handler=None):
        self.user_id = user_id
        self.context = context
        self.db = db
        self.message_info = message_info
        self._handler = handler

    def on_load(self, prev_menu):
        self.send_message(
            vk_id=self.message_info["from_id"],
            message="Hello, you were directed to menu {} from {}\n{}\n{}".format(
                self.type,
                prev_menu.type,
                json.dumps(self.message_info),
                json.dumps(self.context),
            ),
        )

    def on_execute(self):
        self.send_message(
            vk_id=self.message_info["from_id"],
            message="Hello, you are using menu {}\n{}\n{}".format(
                self.type,
                json.dumps(self.message_info),
                json.dumps(self.context),
            ),
        )

    def send_message(self, vk_id, message, **kwargs):
        api.messages.send(access_token=VK_API_TOKEN, user_id=vk_id, message=message, **kwargs)

    def reply(self, message, **kwargs):
        self.send_message(self.get_vk_id(), message, **kwargs)

    def get_vk_id(self):
        return self.message_info["from_id"]

    def load_menu(self, menu_to_load, input_context=None, call_load=True):
        if input_context is None:
            input_context = dict()
        menu = menu_to_load(
            user_id=self.user_id,
            db=self.db,
            context=input_context,
            message_info=self.message_info,
            handler=self._handler,
        )

        self._handler.current_menu = menu
        utils.log_info("Loaded menu {}", menu.type)
        if call_load:
            menu.on_load(self)


class NotFoundMenu(BaseMenu):
    type = "NOT_FOUND_MENU"
    not_found = None
    menu_to_redirect = None

    def on_execute(self):
        self.reply("Menu type {} is not found, please check your code".format(self.not_found))
        self.load_menu(self.menu_to_redirect)


class MessageHandler(object):
    def __init__(self, menu_list):
        self._menu_list = menu_list
        self.current_menu = None
        self.menu_index = self.index_menus(menu_list)

    def index_menus(self, menu_list):
        index = dict()
        for menu in menu_list:
            if menu.type in index:
                raise MessageHandlerException("Menu type {} is not unique".format(menu.type))
            index[menu.type] = menu
        return index

    def handle(self, message_info):
        with DbRequests.MusicGameDatabase() as db:
            vk_id = message_info["from_id"]
            utils.log_info("Handling message from {}", vk_id)
            player_info = db.get_player_info(vk_id)
            if player_info is None:
                db.insert_player_info(vk_id, menu_name="FIRST_TIME_MENU")
                utils.log_info("User {} is added in database", vk_id)
                player_info = db.get_player_info(vk_id)
            user_id, vk_id, menu_name, menu_context = player_info
            menu_context = json.loads(menu_context)

            current_menu_class = self.menu_index.get(menu_name)
            if current_menu_class is None:
                NotFoundMenu.not_found = menu_name
                NotFoundMenu.menu_to_redirect = self._menu_list[0]
                current_menu_class = NotFoundMenu

            self.current_menu = current_menu_class(
                user_id=user_id,
                db=db,
                context=menu_context,
                message_info=message_info,
                handler=self,
            )
            utils.log_info("Using menu with type {}", self.current_menu.type)
            self.current_menu.on_execute()

            db.update_player_info(
                user_id=user_id,
                menu_name=self.current_menu.type,
                menu_context=json.dumps(self.current_menu.context),
            )
            utils.log_info("All data was saved, now user in menu {}", self.current_menu.type)
            db.commit()
