#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sdk
from random import randint


class FirstTimeMenu(sdk.BaseMenu):
    type = "FIRST_TIME_MENU"

    def on_load(self, prev_menu):
        pass

    def on_execute(self):
        self.reply(
            "Привет, ты у нас впервые, у нас ты можешь поиграть в игру угадай музыку," +
            " чтобы начать играть напиши \"давай поиграем\""
        )
        self.load_menu(NotPlayingMenu, call_load=False)


class NotPlayingMenu(sdk.BaseMenu):
    type = "NOT_PLAYING_MENU"

    def on_load(self, prev_menu):
        self.reply("Вы успешно вышли из игры")

    def on_execute(self):
        text = self.message_info["text"]

        if text.lower() == "давай поиграем":
            self.load_menu(MusicGameMenu)
        elif text.lower() == "name_songs":
            self.load_menu(MusicSetNameMenu)
        elif text.lower() in {"jojo", "жожа", "жижа", "жожо", "джо джо", "jo jo", "джоджо"}:
            self.reply(
                message="Я знал, что ты крутой парень!",
                attachment="video-102087446_456246234",
            )
        else:
            self.send_message(
                vk_id=self.message_info["from_id"],
                message="Если ты вдруг забыл, то чтобы начать игру, нужно написать \"давай поиграем\"",
            )


class MusicGameMenu(sdk.BaseMenu):
    type = "MUSIC_GAME_MENU"

    def on_load(self, prev_menu):
        self.send_music()

    def send_music(self):
        music = self.db.get_random_songs(6)
        chosen = randint(1, 6)
        self.context["answer"] = chosen
        self.send_message(
            vk_id=self.message_info["from_id"],
            message=(
                    "Вот твоя песня и 6 вариантов ответа, напиши номер правильного и ты будешь молодцом, если тебе надоесть, напиши \"хватит\"\n" +
                    "\n".join(["{}. ".format(i + 1) + song[1] for i, song in enumerate(music)])
            ),
            attachment="audio" + music[chosen - 1][2],
        )

    def on_execute(self):
        text = self.message_info["text"]
        if text.lower() == "хватит":
            self.load_menu(NotPlayingMenu)
        else:
            if text in {"1", "2", "3", "4", "5", "6"}:
                if self.context["answer"] == int(text):
                    self.send_message(
                        vk_id=self.message_info["from_id"],
                        message="🎉🎉🎉🎉 ПОЗДРАВЛЯЮ, а ты молодец, угадал музыку 🎉🎉🎉🎉",
                        attachment="photo-157942650_456239017",
                    )
                else:
                    self.send_message(
                        vk_id=self.message_info["from_id"],
                        message="Как же так, ты не угадал...",
                        attachment="photo-157942650_456239018",
                    )
                self.send_music()
            else:
                self.send_message(
                    vk_id=self.message_info["from_id"],
                    message="Введи цифру от 1 до 4, глупенький, ну а если тебе надоело, напиши \"хватит\"",
                )


class MusicSetNameMenu(sdk.BaseMenu):
    type = "MUSIC_SET_NAME_MENU"

    def on_load(self, prev_menu):
        self.send_music()

    def send_music(self):
        music_info = self.db.get_unnamed_song()
        if music_info is None:
            self.reply("There is no unnamed music")
        else:
            music_id, name, vk_id = music_info
            self.context["music_id"] = music_id
            self.send_message(
                vk_id=self.message_info["from_id"],
                message="Напиши название этой песни, \"stop\" - чтобы закончить",
                attachment="audio" + vk_id,
            )

    def on_execute(self):
        text = self.message_info["text"]
        if text.lower() == "stop":
            self.load_menu(NotPlayingMenu)
        else:
            self.db.update_song_info(self.context["music_id"], text)
            self.reply("Принято, название \"{}\"".format(text))
            self.send_music()
