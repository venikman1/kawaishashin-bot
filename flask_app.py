#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from flask import Flask
from flask import request
from menus import MusicGame
import sys
from random import randint
import time
from config import VK_CONFIRMATION_TOKEN
import sdk
import utils

app = Flask(__name__)


@app.route('/', methods=['GET'])
def hello_world():
    return 'Bot is working ^_^'


start_message = 'Привет, я бот, с которым ты можешь поиграть в угадай мелодию. Чтобы начать играть, просто напиши ' \
                '"начать игру". Если вам надоест, просто напишите "хватит". '
rep_message = 'Напоминаю, чтобы начать игру, напишите "начать игру".'


def gen_songs(cursor, player_id):
    cursor.execute("SELECT id, vk_id, name FROM music ORDER BY RAND() LIMIT 4")
    i = 1
    names = []
    ids = []
    for music_id, vk_id, name in cursor.fetchall():
        names.append(name)
        ids.append(vk_id)
        #cursor.execute("INSERT INTO player_music (player_id, music_id, music_num) VALUES (%d, %d, %d)", (player_id, music_id, i,))
        i += 1
    answer = randint(1, 4)
    print("This is player_id: "+repr(player_id), file=sys.stderr)
    cursor.execute("INSERT INTO answer (player_id, music_num) VALUES (%s, %s)", (player_id, answer,))
    return names, ids[answer - 1]


def clear_songs(cursor, player_id):
    cursor.execute("DELETE FROM player_music WHERE player_id = %s", (player_id,))
    cursor.execute("DELETE FROM answer WHERE player_id = %s", (player_id,))


@app.route('/', methods=['POST'])
def processing():
    data = json.loads(request.data.decode())
    now_time = int(time.time())
    if 'type' not in data.keys():
        return 'not vk'
    if data['type'] == 'confirmation':
        utils.log_info("Send confirmation token to vk")
        return VK_CONFIRMATION_TOKEN
    if data['type'] == 'message_new':
        utils.log_info("Getting new message")
        handler = sdk.MessageHandler([
            MusicGame.FirstTimeMenu,
            MusicGame.NotPlayingMenu,
            MusicGame.MusicGameMenu,
            MusicGame.MusicSetNameMenu,
        ])
        handler.handle(data["object"])
        utils.log_info("Message handle complete")
        return "ok"
        # mes_time = data['object']['date'] Кароч если вам интересно почему тут этот код, то просто потому, что я не хочу его удалять
        # if now_time - mes_time > 20: Так что пусть будет)
        #     return 'ok'
        # session = vk.Session()
        # api = vk.API(session, v=5.0)
        # user_id = str(data['object']['user_id'])
        # message = data['object']['body']
        # db = MySQLdb.connect(host="kawaishashinbot.mysql.pythonanywhere-services.com", user="kawaishashinbot", passwd="ilovemusic",
        #                                                                                         db="kawaishashinbot$music", charset='utf8')
        # cursor = db.cursor()
        # cursor.execute("SELECT id, status FROM player WHERE vk_id = %s", (user_id,))
        # r = cursor.fetchone()
        # if r is None:
        #     api.messages.send(access_token=token, user_id=user_id, message=start_message)#, attachment="audio-157942650_456239017") #"video-102087446_456246234")
        #     cursor.execute("INSERT INTO player (vk_id, status) VALUES (%s, 'not_guessing')", (user_id,))
        # elif r[1] == 'guessing':
        #     if message.lower() == "хватит":
        #         clear_songs(cursor, r[0])
        #         cursor.execute("UPDATE player SET status = 'not_guessing' where vk_id = %s", (user_id,))
        #         api.messages.send(access_token=token, user_id=user_id, message="Вы успешно вышли из игры.")
        #     elif message not in {'1', '2', '3', '4'}:
        #         api.messages.send(access_token=token, user_id=user_id, message="Введите цифру для ответа. Если вы хотите выйти, введите \"хватит\".")
        #     else:
        #         cursor.execute("SELECT music_num FROM answer WHERE player_id = %s", (r[0],))
        #         ans = cursor.fetchone()[0]
        #         if int(message) == ans:
        #             text = "Вау, да вы угадали :3"
        #         else:
        #             text = "Неа, вы не угадали :с"#, ответ был " + str(ans)
        #         clear_songs(cursor, r[0])
        #         names, vk_id = gen_songs(cursor, r[0])
        #         api.messages.send(access_token=token, user_id=user_id, message=text + "\nДля ответа введите номер:\n"+"\n".join([str(i + 1) + "." + names[i] for i in range(4)]),
        #             attachment="audio" + vk_id)
        # else:
        #     if message.lower() == 'начать игру':
        #         cursor.execute("UPDATE player SET status = 'guessing' where vk_id = %s", (user_id,))
        #         names, vk_id = gen_songs(cursor, r[0])
        #         api.messages.send(access_token=token, user_id=user_id, message="Для ответа введите номер:\n"+"\n".join([str(i + 1) + "." + names[i] for i in range(4)]),
        #             attachment="audio" + vk_id)
        #     else:
        #         api.messages.send(access_token=token, user_id=user_id, message=rep_message)#, attachment="audio-157942650_456239017") #"video-102087446_456246234")
        # db.commit()
        # db.close()
        # #api.messages.send(access_token=token, user_id=str(user_id), message='"' + message + '" - так говорят только сливальщики АСМ')
        # return 'ok'
