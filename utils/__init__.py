#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys


def log_info(msg, *args, **kwargs):
    print(msg.format(*args, **kwargs), file=sys.stderr)
